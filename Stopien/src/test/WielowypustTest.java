package test;

import static org.junit.Assert.*;

import org.junit.Test;

import polaczenie.Polaczenie;
import polaczenie.Wielowypust;
import stopien.StopienOtworu;
import stopien.StopienWalka;
import wymiar.Tolerancja;
import wymiar.Wymiar;

public class WielowypustTest {

	@Test
	public void wielowypustTest() {
		Wymiar dlugoscOtworu = new Wymiar(15.4d, new Tolerancja(2.1d, 2.2d));
		Wymiar srednicaOtworu = new Wymiar(23d, new Tolerancja(2.1d, 2.2d));
		StopienOtworu stopienOtworu = new StopienOtworu(dlugoscOtworu, srednicaOtworu);
		Wymiar dlugoscWalka = new Wymiar(15.4d, new Tolerancja(2.1d, 2.2d));
		Wymiar srednicaWalka = new Wymiar(26d, new Tolerancja(2.1d, 2.2d));
		StopienWalka stopienWalka = new StopienWalka(dlugoscWalka, srednicaWalka);
		Wymiar srednicaZew = new Wymiar(26d, new Tolerancja(2.1d, 2.2d));
		Wymiar srednicaWew = new Wymiar(23d, new Tolerancja(2.1d, 2.2d));
		Wymiar szerokoscWypustu = new Wymiar(6d, new Tolerancja(0.1d, 0.2d));
		Wymiar dlugosc = new Wymiar(15d, new Tolerancja(0.2d, 0.3d));

		Wielowypust polaczenieWielowypustowe = new Wielowypust(stopienOtworu, stopienWalka, dlugosc, srednicaZew,
				srednicaWew, 6, szerokoscWypustu);

		assertEquals(6, polaczenieWielowypustowe.getLiczbaWypustow());
		assertEquals(26d, polaczenieWielowypustowe.getSrednicaZew().getNominal(), 0d);
	}

	@Test
	public void polaczenieTest() {
		Wymiar dlugoscOtworu = new Wymiar(15.4, new Tolerancja(2.1, 2.2));
		Wymiar srednicaOtworu = new Wymiar(15.2, new Tolerancja(2.1, 2.2));
		StopienOtworu stopienOtworu = new StopienOtworu(dlugoscOtworu, srednicaOtworu);
		Wymiar dlugoscWalka = new Wymiar(15.4, new Tolerancja(2.1, 2.2));
		Wymiar srednicaWalka = new Wymiar(15.2, new Tolerancja(2.1, 2.2));
		StopienWalka stopienWalka = new StopienWalka(dlugoscWalka, srednicaWalka);
		Polaczenie polaczenie = new Polaczenie(stopienOtworu, stopienWalka);

		assertEquals(15.4d, polaczenie.getStopienOtworu().getDlugosc().getNominal(), 0.0d);
	}
}
