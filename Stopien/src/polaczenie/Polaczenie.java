package polaczenie;

import stopien.StopienOtworu;
import stopien.StopienWalka;

public class Polaczenie {

	private StopienOtworu stopienOtworu;
	private StopienWalka stopienWalka;

	public Polaczenie(StopienOtworu stopienOtworu, StopienWalka stopienWalka) {
		this.stopienOtworu = stopienOtworu;
		this.stopienWalka = stopienWalka;
	}

	public StopienOtworu getStopienOtworu() {
		return stopienOtworu;
	}

	public void setStopienOtworu(StopienOtworu stopienOtworu) {
		this.stopienOtworu = stopienOtworu;
	}

	public StopienWalka getStopienWalka() {
		return stopienWalka;
	}

	public void setStopienWalka(StopienWalka stopienWalka) {
		this.stopienWalka = stopienWalka;
	}

}
