package polaczenie;

import stopien.StopienOtworu;
import stopien.StopienWalka;
import wymiar.Wymiar;

public class PolaczenieKsztaltowe extends Polaczenie {

	private Wymiar dlugosc;

	public PolaczenieKsztaltowe(StopienOtworu stopienOtworu, StopienWalka stopienWalka, Wymiar dlugosc) {
		super(stopienOtworu, stopienWalka);
		this.dlugosc = dlugosc;

		// TODO Auto-generated constructor stub
	}

	public Wymiar getDlugosc() {
		return dlugosc;
	}

	public void setDlugosc(Wymiar dlugosc) {
		this.dlugosc = dlugosc;
	}

}
