package polaczenie;

import stopien.StopienOtworu;
import stopien.StopienWalka;
import wymiar.Wymiar;

public class Wielowypust extends PolaczenieKsztaltowe {

	private Wymiar srednicaZew;
	private Wymiar srednicaWew;
	private int liczbaWypustow;
	private Wymiar szerokoscWypustu;

	public Wielowypust(StopienOtworu stopienOtworu, StopienWalka stopienWalka, Wymiar dlugosc, Wymiar srednicaZew, Wymiar srednicaWew,
			int liczbaWypustow, Wymiar szerokoscWypustu) {
		super(stopienOtworu, stopienWalka, dlugosc);
		this.srednicaZew = srednicaZew;
		this.srednicaWew = srednicaWew;
		this.liczbaWypustow = liczbaWypustow;
		this.szerokoscWypustu = szerokoscWypustu;
	}


	public Wymiar getSrednicaZew() {
		return srednicaZew;
	}

	public void setSrednicaZew(Wymiar srednicaZew) {
		this.srednicaZew = srednicaZew;
	}

	public Wymiar getSrednicaWew() {
		return srednicaWew;
	}

	public void setSrednicaWew(Wymiar srednicaWew) {
		this.srednicaWew = srednicaWew;
	}

	public int getLiczbaWypustow() {
		return liczbaWypustow;
	}

	public void setLiczbaWypustow(int liczbaWypustow) {
		this.liczbaWypustow = liczbaWypustow;
	}

	public Wymiar getSzerokoscWypustu() {
		return szerokoscWypustu;
	}

	public void setSzerokoscWypustu(Wymiar szerokoscWypustu) {
		this.szerokoscWypustu = szerokoscWypustu;
	}

}
