package wymiar;

public class Wymiar {
	private double nominal;
	private Tolerancja tolerancja;

	public Wymiar(double nominal, Tolerancja tolerancja) {
		this.nominal = nominal;
		this.tolerancja = tolerancja;
	}

	public double getNominal() {
		return nominal;
	}

	public void setNominal(double nominal) {
		this.nominal = nominal;
	}

	public Tolerancja getTolerancja() {
		return tolerancja;
	}

	public void setTolerancja(Tolerancja tolerancja) {
		this.tolerancja = tolerancja;
	}

}
