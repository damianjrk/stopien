package wymiar;

public class Tolerancja {
	private	double odchylkaGorna;
	private double odchylkaDolna;

	public Tolerancja(double odchylkaGorna, double odchylkaDolna) {
		super();
		this.odchylkaGorna = odchylkaGorna;
		this.odchylkaDolna = odchylkaDolna;
	}

	public double getOdchylkaGorna() {
		return odchylkaGorna;
	}

	public void setOdchylkaGorna(double odchylkaGorna) {
		this.odchylkaGorna = odchylkaGorna;
	}

	public double getOdchylkaDolna() {
		return odchylkaDolna;
	}

	public void setOdchylkaDolna(double odchylkaDolna) {
		this.odchylkaDolna = odchylkaDolna;
	}
	
	
	

}
