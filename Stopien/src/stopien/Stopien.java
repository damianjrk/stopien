package stopien;

import wymiar.Wymiar;

public class Stopien {
	private Wymiar dlugosc;
	private Wymiar srednica;

	public Stopien(Wymiar dlugosc, Wymiar srednica) {
		this.dlugosc = dlugosc;
		this.srednica = srednica;
	}

	public Wymiar getDlugosc() {
		return dlugosc;
	}

	public void setDlugosc(Wymiar dlugosc) {
		this.dlugosc = dlugosc;
	}

	public Wymiar getSrednica() {
		return srednica;
	}

	public void setSrednica(Wymiar srednica) {
		this.srednica = srednica;
	}
}
