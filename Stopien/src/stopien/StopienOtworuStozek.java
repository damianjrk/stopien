package stopien;

import wymiar.Wymiar;

public class StopienOtworuStozek extends StopienOtworu {

	private double zbieznosc;

	public StopienOtworuStozek(Wymiar dlugosc, Wymiar srednica, double zbieznosc) {
		super(dlugosc, srednica);
		this.zbieznosc = zbieznosc;
		// TODO Auto-generated constructor stub
	}

	public double getZbieznosc() {
		return zbieznosc;
	}

	public void setZbieznosc(double zbieznosc) {
		this.zbieznosc = zbieznosc;
	}

}
